import axios from "axios";
import {API_URL} from './../../services/Api';

export const LOGIN_URL = `${API_URL}/auth/signin`;
export const REGISTER_URL = "api/auth/register";
export const REQUEST_PASSWORD_URL = `${API_URL}/auth/esqueci-minha-senha`;
export const REQUEST_ALTER_PASSWORD_URL = `${API_URL}/auth/alter-password`;


export const ME_URL = `${API_URL}/user/me`;

export function login(usernameOrEmail, password) {
  return axios.post(LOGIN_URL, { usernameOrEmail, password });
}

export function register(email, fullname, username, password) {
  return axios.post(REGISTER_URL, { email, fullname, username, password });
}

export function requestPassword(email) {
  return axios.post(REQUEST_PASSWORD_URL, { email });
}

export function alterPassword(password, identify, type) {
  return axios.post(REQUEST_ALTER_PASSWORD_URL, { password, identify, type });
}

export function getUserByToken() {
  return axios.get(ME_URL);
}
